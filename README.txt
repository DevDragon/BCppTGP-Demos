Included with this repository is a collection of short games and demos done as exercises for the book Beginning C++ Through Game Programming. I have included the source C++ files in the source directory and the compiled files (for Linux and Windows) in the builds directory.

Files:
------
+ blackjack
  A simple implementation for 1-7 players of the classic game.

+ critter_caretaker
  A very simple demo of a critter caretaker game where the player can listen to hear the mood of the critter and feed or play with the critter to increase its mood.

+ guess_my_number2
  The classic game of number guessing. In this demo, the player picks a number, and the computer tries to guess.

+ hangman
  Yet another classic game of chance. The computer will pick a word and the player has to guess in time.

+ tic-tac-toe
  An implementation of the game tic-tac-toe with a basic AI implementation.

+ word_jumble
  The computer jumbles a word, and the player must guess the correct word.