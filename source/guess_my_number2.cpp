// Guess My Number
// The classic number guessing game

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main() {
	srand(static_cast<unsigned int>(time(0))); //seed random number generator
	
	int guess = 0;
	int low = 0;
	int high = 101;
	int tries = 1;
	int response;
	
	cout << "\tWelcome to Guess My Number\n\n";
	
	cout << "Think of a number between 1 and 100, then press any key when ready.\n";
	getchar();
	
	while (true) {
		do {
			guess = rand() % 100 + 1; // random number between 1 and 100
		} while (guess <= low || guess >= high);
		
		cout << "My guess is: " << guess << endl;
		cout << "1 - Too high\n2 - Too low\n3 - That's it!\n";
		cin >> response;
		
		if (response == 1) {
			high = guess;
			tries++;
		}
		else if (response == 2) {
			low = guess;
			tries++;
		}
		else if (response == 3) {
			cout << "I got it in " << tries << " guesses!\n";
			break;
		}
	}
	
	return 0;
}